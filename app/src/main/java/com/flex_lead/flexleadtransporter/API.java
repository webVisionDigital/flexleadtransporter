package com.flex_lead.flexleadtransporter;

public class API {

    /*Api base url*/
    public static final String baseUrl = "http://htwistingmill.com/api/";

    /*API values for key*/
    public static final String VALUE_CODE_SUCCESS = "0000";

    public static final String deliverRequest = "delivery_request";
    public static final String deliverRequestTransporter = "delivery_request_transporter";

    /*API key */
    public static final String key_code = "code";
    public static final String keyClientId = "clientId";
    public static final String keyReceiverName = "receiverName";
    public static final String keyReceiverPhone = "receiverPhone";
    public static final String keyWeight = "weight";
    public static final String keyGeoEndLongitude = "geoEndLongitude";
    public static final String keyGeoEndLatitude = "geoEndLatitude";
    public static final String keyGeoStartLongitude = "geoStartLongitude";
    public static final String keyGeoStartLatitude = "geoStartLatitude";


}
