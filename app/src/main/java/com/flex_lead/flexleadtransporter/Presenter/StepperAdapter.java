package com.flex_lead.flexleadtransporter.Presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.flex_lead.flexleadtransporter.View.Fragment.CreateAccount.BasicInfoCreateAccount;
import com.flex_lead.flexleadtransporter.View.Fragment.CreateAccount.IDCreateAccount;
import com.flex_lead.flexleadtransporter.View.Fragment.CreateAccount.PasswordCreateAccount;
import com.flex_lead.flexleadtransporter.View.Fragment.CreateAccount.PictureCreateAccount;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.ArrayList;

public class StepperAdapter extends AbstractFragmentStepAdapter {

    private String[] title;
    private ArrayList<Step> steps;

    public StepperAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);

        title = new String[4];
        steps = new ArrayList<>();

        title[0] = "Basic";
        title[1] = "ID";
        title[2] = "Password";
        title[3] = "Picture";

        steps.add(new BasicInfoCreateAccount());
        steps.add(new IDCreateAccount());
        steps.add(new PasswordCreateAccount());
        steps.add(new PictureCreateAccount());

    }

    @Override
    public Step createStep(int position) {
        return steps.get(position);
    }

    @Override
    public int getCount() {
        return steps.size();
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {

        return new StepViewModel.Builder(context)
                .setTitle(title[position]) //can be a CharSequence instead
                .create();

    }
}
