package com.flex_lead.flexleadtransporter.View.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.flex_lead.flexleadtransporter.Presenter.StepperAdapter;
import com.flex_lead.flexleadtransporter.R;
import com.stepstone.stepper.StepperLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateAccountActivity extends AppCompatActivity {

    @BindView(R.id.stepperLayout)
    StepperLayout stepperLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);

        stepperLayout.setAdapter(new StepperAdapter(getSupportFragmentManager(), this));

    }
}
