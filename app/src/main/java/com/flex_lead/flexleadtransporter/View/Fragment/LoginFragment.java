package com.flex_lead.flexleadtransporter.View.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;

import com.flex_lead.flexleadtransporter.Config;
import com.flex_lead.flexleadtransporter.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BottomSheetDialogFragment {

    private static final String TAG = "Login";
    @BindView(R.id.login)
    Button confirmButton;
    @BindView(R.id.id_login_EditText)
    TextInputEditText loginId;
    @BindView(R.id.password_login_EditText)
    TextInputEditText password;
    @BindView(R.id.id_edit_text_input_layout)
    TextInputLayout loginIdHolder;
    @BindView(R.id.password_edit_text_input_layout)
    TextInputLayout passwordHolder;
    private SharedPreferences sharedPreferences;
    private String receiverNameSaveKey = "receiverNameSaveKey", receiverPhoneSaveKey = "receiverPhoneSaveKey";
    private BottomSheetBehavior<View> bottomSheetBehavior;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                bottomSheetBehavior.setPeekHeight(Config.bottomSheetDefaultPeekHeight);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.login_bottomsheet, null);
        ButterKnife.bind(this, contentView);
        dialog.setContentView(contentView);

        /*if (getActivity() != null)
            sharedPreferences = getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE);*/

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(R.color.bottomSheetBackground));

        if (behavior != null && behavior instanceof BottomSheetBehavior) {

            bottomSheetBehavior = (BottomSheetBehavior<View>) behavior;
            ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_EXPANDED);
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);

        }


    }

    @OnClick({R.id.login})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.login: {

                if (loginId.getText().toString().equals("")) {
                    loginId.setError("Empty");
                }
                if (password.getText().toString().equals("")) {
                    password.setError("Empty");
                }

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                dismiss();

                break;
            }

        }

    }

    /*@Override
    public void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(receiverNameSaveKey, loginId.getText().toString());
        editor.putString(receiverPhoneSaveKey, password.getText().toString());

        editor.apply();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null) {

            loginId.setText(getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE).getString(receiverNameSaveKey, ""));
            password.setText(getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE).getString(receiverPhoneSaveKey, ""));
        }

    }*/
}
