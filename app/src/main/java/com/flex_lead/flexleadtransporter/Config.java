package com.flex_lead.flexleadtransporter;

public class Config {

    /*
     *Shared preference values
     */
    public static final String SP_FlexLead_APP = "flexLead";
    public static final String SP_LOGGED_IN = "loggedIn";

    public static final int bottomSheetDefaultPeekHeight = 150;

}
